package com.nau.lab5;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.*;
import java.net.Socket;

public class Client extends Application {
    private final Display resultDisplay = new Display();
    private final TextField portTf = new TextField();
    private final TextField hostTf = new TextField();
    private final TextField operandTf1 = new TextField();
    private final TextField operandTf2 = new TextField();
    private final Button connectBtn = new Button("Connect");
    private final Button disconnectBtn = new Button("Disconnect");
    private final Button submitBtn = new Button("Submit");
    private final ToggleGroup tg = new ToggleGroup();
    private Thread mainThread = null;
    private Socket socket = null;
    private ClientTask task = null;

    private GridPane buildInputs() {
        GridPane gp = new GridPane();
        String DEFAULT_PORT = "8000";
        String DEFAULT_HOST = "127.0.0.1";

        gp.setHgap(20);
        gp.setVgap(15);
        gp.setLayoutX(250);
        gp.setLayoutY(40);

        Text portText = new Text("Port:");
        Text hostText = new Text("Host:");
        Text operandText1 = new Text("Operand 1:");
        Text operandText2 = new Text("Operand 2:");
        portTf.setMaxWidth(100);
        portTf.setText(DEFAULT_PORT);
        hostTf.setMaxWidth(100);
        hostTf.setText(DEFAULT_HOST);
        operandTf1.setMaxWidth(100);
        operandTf2.setMaxWidth(100);
        connectBtn.setMinWidth(80);
        connectBtn.setTextFill(Color.TOMATO);
        connectBtn.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        disconnectBtn.setMinWidth(80);
        disconnectBtn.setTextFill(Color.TOMATO);
        disconnectBtn.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        disconnectBtn.setDisable(true);
        submitBtn.setMinWidth(80);
        submitBtn.setTextFill(Color.TOMATO);
        submitBtn.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        submitBtn.setDisable(true);


        gp.add(portText, 0, 0);
        gp.add(portTf, 1, 0);
        gp.add(connectBtn, 4, 0);

        gp.add(hostText, 0, 1);
        gp.add(hostTf, 1, 1);
        gp.add(disconnectBtn, 4, 1);

        gp.add(operandText1, 0, 2);
        gp.add(operandTf1, 1, 2);

        gp.add(operandText2, 0, 3);
        gp.add(operandTf2, 1, 3);

        gp.add(submitBtn, 4, 5);

        return gp;
    }

    private GridPane buildOperations() {
        GridPane gp = new GridPane();
        gp.setHgap(40);
        gp.setVgap(10);
        gp.setLayoutX(250);
        gp.setLayoutY(200);

        RadioButton rb1 = new RadioButton(Constant.MINUS);
        RadioButton rb2 = new RadioButton(Constant.SIN);
        RadioButton rb3 = new RadioButton(Constant.COS);
        RadioButton rb4 = new RadioButton(Constant.CTG);

        rb1.setSelected(true);
        rb1.setToggleGroup(tg);
        rb2.setToggleGroup(tg);
        rb3.setToggleGroup(tg);
        rb4.setToggleGroup(tg);

        gp.add(rb1, 0, 0);
        gp.add(rb2, 1, 0);
        gp.add(rb3, 0, 1);
        gp.add(rb4, 1, 1);

        return gp;
    }

    private Pane buildRootPane() {
        Pane mainPane = new Pane();
        resultDisplay.setLayoutX(30);
        resultDisplay.setMaxWidth(200);

        mainPane.getChildren()
            .addAll(
                new Title("Client"),
                resultDisplay,
                buildInputs(),
                buildOperations()
        );
        return mainPane;
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Лабораторная работа №5. Кузнєцов Олексій");
        primaryStage.setResizable(false);

        Pane rootPane = buildRootPane();

        Scene scene = new Scene(rootPane, 610, 290, Color.TRANSPARENT);
        primaryStage.setScene(scene);
        primaryStage.show();

        connectBtn.setOnAction((ActionEvent event) -> {
            task = new ClientTask(hostTf.getText(), portTf.getText(), resultDisplay);
            mainThread = new Thread(task);
            mainThread.start();
            disconnectBtn.setDisable(false);
            submitBtn.setDisable(false);
            connectBtn.setDisable(true);
        });

        disconnectBtn.setOnAction((ActionEvent event) -> {
            mainThread.interrupt();
            disconnectBtn.setDisable(true);
            submitBtn.setDisable(true);
            connectBtn.setDisable(false);
            try {
                socket = task.getSocket();
                socket.close();
                resultDisplay.print("<<< Disconnected: " + hostTf.getText() + ":" + portTf.getText());
            } catch (IOException e) {
                e.printStackTrace();
                resultDisplay.print("== Error during closing connection.");
            }
        });

        submitBtn.setOnAction((ActionEvent event) -> {
            OutputStream output;
            BufferedReader inputStream;
            try {
                output = task.getSocket().getOutputStream();
                inputStream = new BufferedReader(new InputStreamReader(task.getSocket().getInputStream()));

                PrintWriter writer = new PrintWriter(output, true);
                RadioButton selectedBtn = (RadioButton) tg.getSelectedToggle();
                writer.println(" " + operandTf1.getText() + ":" + operandTf2.getText() + ":" + selectedBtn.getText());

                while(!task.getSocket().isClosed()) {
                    resultDisplay.print(inputStream.readLine());
                    resultDisplay.print(inputStream.readLine());
                    resultDisplay.print(inputStream.readLine());
                    break;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        primaryStage.setOnCloseRequest(event -> {
            mainThread.interrupt();

            System.out.println("exit");
            System.exit(0);
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}
