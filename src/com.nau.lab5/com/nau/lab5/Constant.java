package com.nau.lab5;

public class Constant {
    public static final String MINUS = "Minus";
    public static final String SIN = "Sin";
    public static final String COS = "Cos";
    public static final String CTG = "Ctg";
    public static final int PORT = 8000;
}
