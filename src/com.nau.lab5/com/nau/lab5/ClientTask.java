package com.nau.lab5;

import javafx.application.Platform;

import java.net.Socket;

public class ClientTask implements Runnable {
    private final String host;
    private final int port;
    private final Display display;
    private Socket socket = null;

    public ClientTask(String host, String port, Display display) {
        this.host = host;
        this.port = Integer.parseInt(port);
        this.display = display;
    }

    public Socket getSocket() {
        return socket;
    }

    @Override
    public void run() {
        try {
            socket = new Socket(host, port);
            Platform.runLater(() -> display.print(">>> Connected: " + host + ":" + port));
        } catch (Exception e) {
            Platform.runLater(() -> display.print("== Connection failed: " + host + ":" + port));
        }
    }
}
