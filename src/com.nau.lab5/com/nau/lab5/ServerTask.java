package com.nau.lab5;

import javafx.concurrent.Task;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerTask extends Task {
    private ServerSocket serverSocket;
    private final Display connectionDisplay;
    private final Display resultDisplay;
    private final Text connectionNumber;

    public ServerTask(ServerSocket socket, Display connection, Display result, Text connNum) {
        serverSocket = socket;
        connectionDisplay = connection;
        resultDisplay = result;
        connectionNumber = connNum;
    }
    @Override
    protected Object call() {
        try {
            serverSocket = new ServerSocket(Constant.PORT);

            while (true) {
                Socket socket = serverSocket.accept();
                new WorkerThread(socket, connectionDisplay, resultDisplay, connectionNumber).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
            this.cancel();
        }
        return null;
    }
}
