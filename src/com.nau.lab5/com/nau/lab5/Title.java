package com.nau.lab5;

import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class Title extends Text {
    public Title(String txt) {
        super(txt);
        this.setFill(Color.BLUE);
        this.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        this.setLayoutX(40);
        this.setLayoutY(30);
    }
}
