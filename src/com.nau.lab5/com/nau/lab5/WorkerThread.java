package com.nau.lab5;

import javafx.scene.text.Text;

import java.io.*;
import java.net.Socket;

public class WorkerThread extends Thread {
    private final Socket socket;
    private final Display connectionDisplay;
    private final Text connections;
    private final Display resultDisplay;

    public WorkerThread(Socket socket, Display connectionDisplay, Display resultDisplay, Text connections) {
        this.socket = socket;
        this.connections = connections;
        this.connectionDisplay = connectionDisplay;
        this.resultDisplay = resultDisplay;
    }

    private int[] parseOperators(String input) {
        String[] operators = input.split(":");
        return new int[]{Integer.parseInt(operators[0]), Integer.parseInt(operators[1])};
    }

    private String parseAction(String input) {
        return input.split(":")[2];
    }

    private void incrementConnection() {
        int num = Integer.parseInt(connections.getText());
        connections.setText("" + (++num));
    }

    private void decrementConnection() {
        int num = Integer.parseInt(connections.getText());
        connections.setText("" + (--num));
    }

    private String calculateResult(int[] operators, String action) {
        switch (action) {
            case Constant.MINUS -> {
                return "" + (operators[0] - operators[1]) + "\n";
            }
            case Constant.COS -> {
                return "" + Math.cos(operators[0]) + "\n" + Math.cos(operators[1]);
            }
            case Constant.SIN -> {
                return "" + Math.sin(operators[0]) + "\n" + Math.sin(operators[1]);
            }
            case Constant.CTG -> {
                return "" + (Math.cos(operators[0]) / Math.sin(operators[0])) + "\n" + (Math.cos(operators[1]) / Math.sin(operators[1]));
            }
            default -> {
                return "Error: not supported actions.";
            }
        }
    }

    public void run() {
        connectionDisplay.print(">>> Connected: " + socket.getLocalPort());
        incrementConnection();
        BufferedReader inputStream;
        try {
            inputStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            while(!socket.isClosed()) {
                if (inputStream.read() == -1) {
                    connectionDisplay.print("<<< Disconnected: " + socket.getLocalPort());
                    decrementConnection();
                    socket.close();
                }
                String input = inputStream.readLine();
                if (input != null && !input.trim().equals("")){
                    int[] operators = parseOperators(input);
                    String action = parseAction(input);
                    String result = calculateResult(operators, action);
                    resultDisplay.print("Operators:");
                    resultDisplay.print(Integer.toString(operators[0]));
                    resultDisplay.print(Integer.toString(operators[1]));
                    resultDisplay.print(action + ":");
                    resultDisplay.print(result);

                    OutputStream output = socket.getOutputStream();
                    PrintWriter writer = new PrintWriter(output, true);
                    writer.println(action + ":\n" + result);
                }
            }
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
