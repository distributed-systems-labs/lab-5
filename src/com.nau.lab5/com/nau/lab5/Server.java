package com.nau.lab5;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.ServerSocket;

public class Server extends Application {
    private final Display connectionDisplay = new Display();
    private final Display resultDisplay = new Display();
    private final Text numberCon = new Text("" + 0);
    private final ServerSocket serverSocket = null;

    private Pane buildRootPane() {
        Pane mainPane = new Pane();
        Text port = new Text("Port: " + Constant.PORT);
        Text connections = new Text("Connections:");

        port.setLayoutX(330);
        port.setLayoutY(30);
        connections.setLayoutX(450);
        connections.setLayoutY(30);
        numberCon.setLayoutX(530);
        numberCon.setLayoutY(30);
        resultDisplay.setLayoutX(30);
        connectionDisplay.setLayoutX(320);

        mainPane
            .getChildren()
            .addAll(
                new Title("Server"),
                resultDisplay,
                connectionDisplay,
                port,
                connections,
                numberCon
        );
        return mainPane;
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Лабораторная работа №5. Кузнєцов Олексій");
        primaryStage.setResizable(false);

        Pane rootPane = buildRootPane();

        Scene scene = new Scene(rootPane, 610, 290, Color.TRANSPARENT);
        primaryStage.setScene(scene);
        primaryStage.show();

        ServerTask mainTask = new ServerTask(serverSocket, connectionDisplay, resultDisplay, numberCon);

        Thread mainThread = new Thread(mainTask);
        mainThread.start();

        primaryStage.setOnCloseRequest(event -> {
            mainThread.interrupt();
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("exit");
            System.exit(0);
        });

    }

    public static void main(String[] args) {
        launch(args);
    }
}
